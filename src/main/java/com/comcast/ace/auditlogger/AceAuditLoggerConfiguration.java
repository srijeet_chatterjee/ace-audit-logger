package com.comcast.ace.auditlogger;

import com.datasift.dropwizard.kafka.KafkaConsumerFactory;
import com.datasift.dropwizard.kafka.PollingProcessorFactory;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.logging.DefaultLoggingFactory;
import io.dropwizard.logging.LoggingFactory;
import org.hibernate.validator.constraints.NotEmpty;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by schatt204 on 11/17/16.
 */
public class AceAuditLoggerConfiguration extends Configuration
{
    @NotNull
    @Valid
    private KafkaConsumerFactory kafkaConsumerFactory = new KafkaConsumerFactory();

    @NotNull
    @Valid
    private PollingProcessorFactory pollingProcessorFactory = new PollingProcessorFactory();

    @JsonProperty("logging")
    public Logger getAppLogger() {return LoggerFactory.getLogger("app_logs");}

    @JsonProperty("logging")
    public Logger getErrorLogger() {return LoggerFactory.getLogger("error_logs");}

    @JsonProperty("kafka-consumer")
    public KafkaConsumerFactory getKafkaConsumerFactory() {
        return kafkaConsumerFactory;
    }

    @JsonProperty("kafka-consumer")
    public void setKafkaConsumerFactory(KafkaConsumerFactory kafkaConsumerFactory) {
        this.kafkaConsumerFactory = kafkaConsumerFactory;
    }

    @JsonProperty("polling-processor")
    public PollingProcessorFactory getPollingProcessorFactory() { return pollingProcessorFactory; }

    @JsonProperty("polling-processor")
    public void setPollingProcessorFactory(PollingProcessorFactory pollingProcessorFactory) {
        this.pollingProcessorFactory = pollingProcessorFactory;
    }

    @NotEmpty
    private String logRootFile = "";

    @JsonProperty
    public String getLogRootFile()
    {
        return logRootFile;
    }

    @JsonProperty
    public void setLogRootFile(String logRootFile)
    {
        this.logRootFile = logRootFile;
    }
}
