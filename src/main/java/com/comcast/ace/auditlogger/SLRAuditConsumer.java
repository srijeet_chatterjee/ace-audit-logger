package com.comcast.ace.auditlogger;

/**
 * Consumes the data coming in from the stream, decodes the device ID and the body of the message,
 * and sends it to the audit Log Writer class to be written into the appropriate folder.
 * Created by Christopher Brown on 7/29/16.
 */
import com.comcast.ace.mnemosyne.api.AuditMessage;
import com.datasift.dropwizard.kafka.consumer.ConsumerRecordProcessor;

import java.io.*;
import java.util.Iterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.avro.AvroFactory;
import com.fasterxml.jackson.dataformat.avro.AvroSchema;
import headwaters.mnemosyne.AuditEvent;
import org.apache.avro.Schema;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.joda.time.DateTime;
import java.net.HttpURLConnection;
import java.net.URL;

public class SLRAuditConsumer implements Runnable, ConsumerRecordProcessor<byte[], byte[]>
{
    private ConsumerRecords<byte[], byte[]> records;
    private String topic;

    private static AceAuditLoggerConfiguration configObject        = new AceAuditLoggerConfiguration();
    final static org.slf4j.Logger logger              = configObject.getAppLogger();
    final static org.slf4j.Logger errorLogger         = configObject.getErrorLogger();

    public enum MessageType
    {
        ADS_REQUEST("Ads Request"),
        ADS_RESPONSE("Ads Response"),
        INCOMING_PSN("IncomingPSN"),
        PLACEMENT_REQUEST("Placement Request"),
        PLACEMENT_RESPONSE("Placement Response");

        private String value;

        MessageType(String value)
        {
            this.value = value;
        }

        private String getValue()
        {
            return value;
        }
    }

    public SLRAuditConsumer(ConsumerRecords<byte[], byte[]> myRecords, String topic)
    {
        this.records = myRecords;
        this.topic = topic;
    }

    private static String sendGet() throws Exception
    {
        String            url          = "http://headwaters.sandbox.video.comcast.net/schema_registry/schemas/ids/1128819292";
        int               responseCode = 0;
        URL               obj          = new URL(url);
        HttpURLConnection con          = (HttpURLConnection) obj.openConnection();
        BufferedReader    in           = null;
        String            inputLine    = null;
        StringBuffer      response     = null;

        con.setRequestMethod("GET");
        responseCode = con.getResponseCode();
        if(responseCode == 200)
        {
            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            response = new StringBuffer();

            while (null != (inputLine = in.readLine()))
            {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
        }
        else
        {
            errorLogger.error("Error in response : code : " + responseCode);
            return null;
        }
    }

    public static AuditMessage convertEventToMsg(AuditEvent event)
    {
        AuditMessage msg = new AuditMessage();
        if ((null != event) && (null != msg))
        {
            msg.setBody(event.getBody());
            msg.setAuditId(event.getCorrelationId());
            msg.setHttpRequest(event.getHttpRequest());
            if (null != event.getHeader())
            {
                if (null != event.getHeader().getMoney())
                {
                    if (null != event.getHeader().getMoney().getSpanName())
                    {
                        msg.setCategory(event.getHeader().getMoney().getSpanName().toString());
                    }
                }
                if (null != event.getHeader().getTimestamp())
                {
                    msg.setTimeStamp(new DateTime(event.getHeader().getTimestamp().getValue()));
                }
            }
        }
        return msg;
    }

    public void process(ConsumerRecords consumerRecords)
    {
        String JSON_SCHEMA = null;
        try
        {
            JSON_SCHEMA = SLRAuditConsumer.sendGet();
        }
        catch (Exception e)
        {
            errorLogger.error("Failed to get the schema!!");
            errorLogger.error(e.getStackTrace().toString());
        }

        //TODO: The Get call returns the schema with the word "schema" at the start. This creates problems during parsing, hardcoding it for now

        JSON_SCHEMA         = "{\n  \"type\" : \"record\",\n  \"name\" : \"AuditEvent\",\n  \"namespace\" : \"headwaters.mnemosyne\",\n  \"doc\" : \"An audit event is a raw record used to track placement requests, placement responses and other audit information generated for and used by mnemosyne\",\n  \"fields\" : [ {\n    \"name\" : \"header\",\n    \"type\" : {\n      \"type\" : \"record\",\n      \"name\" : \"CoreHeader\",\n      \"namespace\" : \"headwaters\",\n      \"doc\" : \"Common information related to the event which must be included in any clean event. It allows some common processing at the system level, and some consistency for processing events.\",\n      \"fields\" : [ {\n        \"name\" : \"timestamp\",\n        \"type\" : [ \"null\", {\n          \"type\" : \"record\",\n          \"name\" : \"Timestamp\",\n          \"namespace\" : \"headwaters.datatype\",\n          \"doc\" : \"A UTC time stamp in milliseconds since Unix epoch (January 1, 1970 midnight). For instance, March 7th 2016, 20:00:00 UTC is 1457380800000\",\n          \"fields\" : [ {\n            \"name\" : \"value\",\n            \"type\" : \"long\",\n            \"default\" : 0\n          } ]\n        } ],\n        \"doc\" : \"The UTC time stamp in milliseconds since Unix epoch (January 1, 1970 midnight) when the event is generated.\",\n        \"default\" : null\n      }, {\n        \"name\" : \"uuid\",\n        \"type\" : [ \"null\", {\n          \"type\" : \"record\",\n          \"name\" : \"UUID\",\n          \"namespace\" : \"headwaters.datatype\",\n          \"doc\" : \"A Universally Unique Identifier, in canonical form in lowercase. Example: de305d54-75b4-431b-adb2-eb6b9e546014\",\n          \"fields\" : [ {\n            \"name\" : \"value\",\n            \"type\" : {\n              \"type\" : \"string\",\n              \"avro.java.string\" : \"String\"\n            },\n            \"default\" : \"\"\n          } ]\n        } ],\n        \"doc\" : \"Unique identifier for the event used for de-duplication and tracing.\",\n        \"default\" : null\n      }, {\n        \"name\" : \"hostname\",\n        \"type\" : [ \"null\", {\n          \"type\" : \"string\",\n          \"avro.java.string\" : \"String\"\n        } ],\n        \"doc\" : \"Fully qualified name of the host that generated the event that generated the data.\",\n        \"default\" : null\n      }, {\n        \"name\" : \"money\",\n        \"type\" : [ \"null\", {\n          \"type\" : \"record\",\n          \"name\" : \"Money\",\n          \"doc\" : \"Money trace information. Encapsulates the money identifiers and additional keys that the analytics sender selects to be part of this object. \",\n          \"fields\" : [ {\n            \"name\" : \"traceId\",\n            \"type\" : [ \"null\", \"headwaters.datatype.UUID\" ],\n            \"doc\" : \"Money Trace Identifier. The unique identifier of a Money Trace (i.e. the top-level object in Money). All the Spans related to the Trace hold this same UUID. It corresponds to the Money trace-id.\",\n            \"default\" : null\n          }, {\n            \"name\" : \"spanId\",\n            \"type\" : [ \"null\", \"long\" ],\n            \"doc\" : \"Money Span Identifier. The identifier of the Span (i.e. one step of the Money Trace). It is a random signed long that represents a unique id for the current Span. It corresponds to the Money span-id.\",\n            \"default\" : null\n          }, {\n            \"name\" : \"parentSpanId\",\n            \"type\" : [ \"null\", \"long\" ],\n            \"doc\" : \"Money Parent Span Identifier. Signed long that represents the direct upstream Span of the current Span. It corresponds to the Money parent-id.\",\n            \"default\" : null\n          }, {\n            \"name\" : \"spanName\",\n            \"type\" : [ \"null\", {\n              \"type\" : \"string\",\n              \"avro.java.string\" : \"String\"\n            } ],\n            \"doc\" : \"A logical, operator-readable name for a span.  Something like CAM.isPlayable, for CAM's isPlayable call, for instance.\",\n            \"default\" : null\n          }, {\n            \"name\" : \"appName\",\n            \"type\" : [ \"null\", {\n              \"type\" : \"string\",\n              \"avro.java.string\" : \"String\"\n            } ],\n            \"doc\" : \"The name of the application that is using money\",\n            \"default\" : null\n          }, {\n            \"name\" : \"startTime\",\n            \"type\" : [ \"null\", \"long\" ],\n            \"doc\" : \"A UTC time stamp in microseconds when the trace started since Unix epoch (January 1, 1970 midnight)\",\n            \"default\" : null\n          }, {\n            \"name\" : \"spanDuration\",\n            \"type\" : [ \"null\", \"long\" ],\n            \"doc\" : \"Duration of the span, in microseconds.\",\n            \"default\" : null\n          }, {\n            \"name\" : \"spanSuccess\",\n            \"type\" : [ \"null\", \"boolean\" ],\n            \"doc\" : \"False if the span experienced an error, true otherwise.\",\n            \"default\" : null\n          }, {\n            \"name\" : \"host\",\n            \"type\" : [ \"null\", {\n              \"type\" : \"string\",\n              \"avro.java.string\" : \"String\"\n            } ],\n            \"doc\" : \"The machine name generating the data, for Money purpose.\",\n            \"default\" : null\n          } ]\n        } ],\n        \"doc\" : \"Money trace information not redundant with this object\",\n        \"default\" : null\n      } ]\n    },\n    \"doc\" : \"Core data information required for any event\"\n  }, {\n    \"name\" : \"correlationId\",\n    \"type\" : {\n      \"type\" : \"string\",\n      \"avro.java.string\" : \"String\"\n    },\n    \"doc\" : \"A correlation id is an identitifer that is shared across all audit events used to tie all audit events together.  A single correlation id is used to match all audit events related to one Ad decision request.  see http://www.enterpriseintegrationpatterns.com/patterns/messaging/CorrelationIdentifier.html.\",\n    \"default\" : \"-\"\n  }, {\n    \"name\" : \"httpRequest\",\n    \"type\" : [ \"null\", {\n      \"type\" : \"string\",\n      \"avro.java.string\" : \"String\"\n    } ],\n    \"doc\" : \"The HTTP Request string used to request some operation\",\n    \"default\" : null\n  }, {\n    \"name\" : \"httpResponseCode\",\n    \"type\" : [ \"null\", \"int\" ],\n    \"doc\" : \"The HTTP response status code generated with this audit message\",\n    \"default\" : null\n  }, {\n    \"name\" : \"body\",\n    \"type\" : [ \"null\", {\n      \"type\" : \"string\",\n      \"avro.java.string\" : \"String\"\n    } ],\n    \"doc\" : \"The body of the request\",\n    \"default\" : null\n  }, {\n    \"name\" : \"context\",\n    \"type\" : [ \"null\", {\n      \"type\" : \"map\",\n      \"values\" : {\n        \"type\" : \"string\",\n        \"avro.java.string\" : \"String\"\n      },\n      \"avro.java.string\" : \"String\"\n    } ],\n    \"doc\" : \"Context map associated with an audit event.  These are optional fields that may or may not be present.\",\n    \"default\" : null\n  } ]\n}";
        Schema       raw    = new Schema.Parser().setValidate(true).parse(JSON_SCHEMA);
        AvroSchema   schema = new AvroSchema(raw);
        ObjectMapper mapper = new ObjectMapper(new AvroFactory());

        Iterable<ConsumerRecord<byte[], byte[]>> list = consumerRecords.records(topic);
        Iterator<ConsumerRecord<byte[], byte[]>> it = list.iterator();

        if (null == it || null == list)
        {
            errorLogger.error("Null in iterator " + it + " or list " + list);
        }
        else
        {
            while (it.hasNext())
            {
                byte[] avroData = it.next().value();

                if((null != avroData) && (avroData.length >= 0))
                {
                    try
                    {
                        AuditEvent event = mapper.reader(AuditEvent.class).with(schema).readValue(avroData);

                        if(null != event)
                        {
                            AuditMessage msg = convertEventToMsg(event);
                            if (msg != null)
                            {
                                if(null != msg.getCategory())
                                {
                                    if (msg.getCategory().equalsIgnoreCase(MessageType.PLACEMENT_REQUEST.getValue())
                                            || msg.getCategory().equalsIgnoreCase(MessageType.PLACEMENT_RESPONSE.getValue())
                                            || msg.getCategory().equalsIgnoreCase(MessageType.INCOMING_PSN.getValue())
                                            || msg.getCategory().equalsIgnoreCase(MessageType.ADS_REQUEST.getValue())
                                            || msg.getCategory().equalsIgnoreCase(MessageType.ADS_RESPONSE.getValue()))
                                    {
                                        if(logger.isInfoEnabled())
                                        {
                                            logger.info(msg.toString());
                                            logger.info("\n\n");
                                        }
                                    }
                                }
                            }
                            else
                            {
                                errorLogger.error("Msg is NULL!");
                            }
                        }
                        else
                        {
                            errorLogger.error("The event is null!");
                        }
                    }
                    catch (IOException e)
                    {
                        errorLogger.error("Could not parse the incoming object with the available schema!");
                        errorLogger.error("\n\n");
                    }
                }
            }
        }
    }

    public void run()
    {
        process(records);
    }
}