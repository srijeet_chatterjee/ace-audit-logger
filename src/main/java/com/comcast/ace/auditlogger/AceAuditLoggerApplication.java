package com.comcast.ace.auditlogger;

import com.datasift.dropwizard.kafka.consumer.ConsumerRecordProcessor;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.slf4j.Logger;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by schatt204 on 11/17/16.
 */

public class AceAuditLoggerApplication extends Application<AceAuditLoggerConfiguration>
{
    private       ExecutorService             executor            = null;
    private static AceAuditLoggerConfiguration configObject        = new AceAuditLoggerConfiguration();
    final static  Logger                      logger              = configObject.getAppLogger();
    final static  Logger                      errorLogger         = configObject.getErrorLogger();

    public static void printUsage()
    {
        logger.info("Usage:");
        logger.info("com.comcast.ace.slraudit.SLRAuditDeviceLogger zookeeper consumer_group_name topic threadCount rootLoggingDir");
        logger.info("Example:");
        logger.info("com.comcast.ace.slraudit.SLRAuditDeviceLogger 172.30.18.210 JavaTest slr-auditing 20 /var/log/captures");
    }

    public static void printStartup(String zookeeper, String groupId, String topic, int threadCount, String logFileRoot)
    {
        SimpleDateFormat logDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        logger.info("Starting SLRAuditDeviceLogger " + logDateFormat.format(new Date()) + "\n");
        logger.info("\tZookeeper     :\t" + zookeeper + "\n");
        logger.info("\tGroup ID      :\t" + groupId + "\n");
        logger.info("\tTopic Name    :\t" + topic + "\n");
        logger.info("\tThread Count  :\t" + threadCount + "\n");
        logger.info("\tLog File Root :\t" +  logFileRoot + "\n");
    }

    public void shutdown()
    {
        if (executor != null) executor.shutdown();
        try
        {
            if (!executor.awaitTermination(5000, TimeUnit.MILLISECONDS))
            {
                errorLogger.error("Timed out waiting for consumer threads to shut down, exiting uncleanly");
            }
        }
        catch (InterruptedException e)
        {
            errorLogger.error("Interrupted during shutdown, exiting uncleanly");
        }
    }

    public static void main(String args[]) throws Exception
    {
        long sleepTime             = 365*24*60*60*1000L;
        AceAuditLoggerApplication example = new AceAuditLoggerApplication();

        if (null == args)
        {
            printUsage();
            System.exit(1);
        }
        if(null != example)
        {
            example.run(args);
        }
        else
        {
            errorLogger.error("Application Object is null!");
        }

        try
        {
            Thread.sleep(sleepTime);
        }
        catch (InterruptedException ie)
        {
            errorLogger.error("Execution Interrupted! Quitting! ");
            ie.printStackTrace();
        }
        example.shutdown();
    }

    @Override
    public void initialize(Bootstrap<AceAuditLoggerConfiguration> bootstrap)
    {
        //Nothing here as of now
    }

    public void run(AceAuditLoggerConfiguration exampleApplicationConfiguration, Environment environment) throws Exception
    {
        if(null == exampleApplicationConfiguration || null == environment)
        {
            errorLogger.error("Config object or environment is null");
        }
        else
        {
            String logFileRoot = exampleApplicationConfiguration.getLogRootFile();
            String zooKeeper   = exampleApplicationConfiguration.getKafkaConsumerFactory().getBrokers().iterator().next().toString();
            int    portno      = exampleApplicationConfiguration.getKafkaConsumerFactory().getBrokers().iterator().next().getPort();
            zooKeeper          = zooKeeper + ":" + Integer.toString(portno);
            String topic       = exampleApplicationConfiguration.getPollingProcessorFactory().getTopics().iterator().next().toString();
            int threads        = exampleApplicationConfiguration.getPollingProcessorFactory().getBatchCount();
            String groupId     = exampleApplicationConfiguration.getKafkaConsumerFactory().getGroup();

            /*Create the auditlogwriter and the SLRauditconsumer to be passed into the processor*/
            final SLRAuditConsumer consumer = new SLRAuditConsumer(null, topic);

            printStartup(zooKeeper,groupId,topic,threads,logFileRoot);

            ByteArrayDeserializer valueDeserializer = new ByteArrayDeserializer();
            final ConsumerRecordProcessor<byte[], byte[]> prcr = new ConsumerRecordProcessor<byte[], byte[]>() {
                public void process(ConsumerRecords<byte[], byte[]> consumerRecords) {
                    consumer.process(consumerRecords);
                }
            };

            final Consumer<byte[], byte[]> kafkaconsumer = exampleApplicationConfiguration.getKafkaConsumerFactory().build(environment, valueDeserializer, valueDeserializer, "consumer");
            exampleApplicationConfiguration.getPollingProcessorFactory().build(environment, kafkaconsumer, prcr, "polling");
        }
    }
}