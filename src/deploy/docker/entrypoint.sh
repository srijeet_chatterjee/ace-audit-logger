#!/bin/bash
set -e

if [ $# -eq 0 ] || [ "${1:0:1}" = '-' ]; then
	exec java "$@" -jar ${JAR_FINAL_NAME} server config.yaml
else
    exec "$@"
fi