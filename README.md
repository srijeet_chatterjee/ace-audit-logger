# Ace-Audit-Logger
This is the repo for the Ace Audit Logger (meant to be the replacement for Mnemosyne), which is compatible with kafka9.

VM args: -Dlog4j.configuration=file:///Users/schatt204/srijeet_repos/ace-audit-logger/src/main/java/com/comcast/ace/auditlogger/resources/log4j.properties

Prog args: server config.yaml

You can change the config.yaml file to point to whichever kafka endpoint.
